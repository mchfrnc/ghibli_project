from django.urls import path, include
from movie_list.urls import urls as movie_list_urls

urlpatterns = [
    path("movies/", include(movie_list_urls)),
]
