from rest_framework import serializers


class MovieListSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    title = serializers.CharField()
    people = serializers.ListField()
