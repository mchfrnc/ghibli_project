from rest_framework.response import Response
from rest_framework.views import APIView

from movie_list.movies_getter import (
    FILMS_ENDPOINT,
    get_films_with_people,
    RemoteApiException,
)
from movie_list.serializers import MovieListSerializer


class MovieList(APIView):
    """
    List all films with people
    """

    def get(self, request, format=None):
        try:
            movie_list = get_films_with_people(FILMS_ENDPOINT)
        except RemoteApiException:
            return Response("Couldn't get films from remote API", status=400)
        serializer = MovieListSerializer(movie_list, many=True)
        return Response(serializer.data)
