from django.urls import path

from movie_list import views

urls = [
    path("", views.MovieList.as_view()),
]
