from typing import List, Union

import requests
import requests_cache

requests_cache.install_cache(
    cache_name="movies_cache", backend="sqlite", expire_after=60
)

GHIBLI_API_URL = "https://ghibliapi.herokuapp.com"
FILMS_ENDPOINT = {"endpoint": "films", "fields": ["id", "title"]}
PEOPLE_ENDPOINT = {"endpoint": "people", "fields": ["name", "films"]}


class RemoteApiException(Exception):
    pass


def client_ghibli(endpoint: str, fields: list = None) -> tuple:
    """
    Client for Ghibli API. Raises exception when status code of response is not OK.

    :param endpoint: Endpoint withing Ghibli API.
    :param fields: Fields to be returned in a response
    :return: Response content and status code
    """
    url = f"{GHIBLI_API_URL}/{endpoint}"
    if fields:
        url += f"?fields={','.join(fields)}"

    res = requests.get(url)
    if res.status_code != requests.codes.ok:
        res.raise_for_status()
    return res.json(), res.status_code


def correlate_films_with_people(people_endpoint: dict) -> Union[dict, None]:
    """
    Gets people from given endpoint and correlates film IDs with names of people, who appear
    in that film.

    :param people_endpoint: A dictionary with defined endpoint's name and fields
    :return: - A dictionary, where film ID is a key, and it's value is a list with names of people from this film;
             - None when there was an error while getting data from given endpoint
    """
    try:
        people, status_code = client_ghibli(
            people_endpoint["endpoint"], people_endpoint["fields"]
        )
    except requests.exceptions.HTTPError:
        return

    films_with_people = {}
    for person in people:
        for film in person.get("films", []):
            film_id = film.split("/")[-1]
            films_with_people.setdefault(film_id, []).append(person["name"])

    return films_with_people


def get_films_with_people(films_endpoint: dict) -> Union[list]:
    """
    To avoid performing excessive requests, firstly we execute a request to /people endpoint, and correlate
    film IDs with people (actors) in them.
    Then we are requesting for /films endpoint, and basing on film IDs correalted with people from previous request, we are
    assigning people to movie titles.
    Thanks for that we are performing only 2 requests to API.

    :param films_endpoint: A dictionary with defined endpoint's name and fields
    :return: A list of films with names of people appearing in those films
    """
    correlated_films_people = correlate_films_with_people(PEOPLE_ENDPOINT)
    if not correlated_films_people:
        return []

    try:
        films, status_code = client_ghibli(
            films_endpoint["endpoint"], films_endpoint["fields"]
        )
    except requests.exceptions.HTTPError:
        raise RemoteApiException(f"Couldn't get films from remote API")

    for (index, film) in enumerate(films):
        film_id = film["id"]
        if film_id in correlated_films_people:
            films[index]["people"] = correlated_films_people[film_id]
        else:
            films[index]["people"] = []

    return films
