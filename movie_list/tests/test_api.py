import pytest

from movie_list.movies_getter import RemoteApiException


def test_get_movie_list(api_client, mocker):
    from movie_list import views

    expected = [
        {
            "id": "0440483e-ca0e-4120-8c50-4c8cd9b965d6",
            "title": "Princess Mononoke",
            "people": ["Ashitaka", "San"],
        },
        {
            "id": "758bf02e-3122-46e0-884e-67cf83df1786",
            "title": "Ponyo",
            "people": ["Sosuke"],
        },
        {
            "id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
            "title": "Castle in the Sky",
            "people": [],
        },
    ]

    mocker.patch.object(views, "get_films_with_people", return_value=expected)
    response = api_client.get("/movies/")

    assert 200 == response.status_code
    assert expected == response.json()


def test_get_movie_failed(api_client, mocker):
    from movie_list import views

    mocker.patch.object(views, "get_films_with_people", side_effect=RemoteApiException)
    response = api_client.get("/movies/")

    assert 400 == response.status_code
    assert response.json() == "Couldn't get films from remote API"


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient

    return APIClient()
