import pytest
import requests

from movie_list.movies_getter import (
    client_ghibli,
    GHIBLI_API_URL,
    correlate_films_with_people,
    get_films_with_people,
)


def test_client_ghibli_nok(requests_mock):
    bad_endpoint = "nonexistingendpoint"
    bad_url = f"{GHIBLI_API_URL}/{bad_endpoint}"
    requests_mock.get(bad_url, status_code=404)

    with pytest.raises(requests.exceptions.HTTPError):
        client_ghibli(bad_endpoint)


def test_client_ghibli_ok(requests_mock):
    ok_endpiont = "okendpoint"
    ok_url = f"{GHIBLI_API_URL}/{ok_endpiont}"
    requests_mock.get(ok_url, status_code=200, json={"a": 1, "b": 2})

    content, status_code = client_ghibli(ok_endpiont)

    assert {"a": 1, "b": 2} == content
    assert 200 == status_code


def test_client_ghibli_with_fields(mocker):
    import requests

    mocker.patch.object(requests, "get")

    endpoint = "endpoint"
    fields = ["field1", "field2"]
    url = f"{GHIBLI_API_URL}/{endpoint}?fields=field1,field2"

    client_ghibli(endpoint, fields)

    requests.get.assert_called_once_with(url)


def test_correlate_films_with_people_bad_request(mocker):
    from movie_list import movies_getter

    mocker.patch.object(
        movies_getter, "client_ghibli", side_effect=requests.exceptions.HTTPError
    )

    res = correlate_films_with_people({"endpoint": "endpoint", "fields": ["1", "2"]})

    assert None is res


def test_correlate_films_with_people_emplty_list(mocker):
    from movie_list import movies_getter

    returned = [], 200
    mocker.patch.object(movies_getter, "client_ghibli", return_value=returned)

    res = correlate_films_with_people(
        {"endpoint": "people", "fields": ["name", "films"]}
    )

    assert {} == res


def test_correlate_films_with_people(mocker):
    from movie_list import movies_getter

    returned = (
        [
            {
                "name": "Ashitaka",
                "films": [
                    "https://ghibliapi.herokuapp.com/films/0440483e-ca0e-4120-8c50-4c8cd9b965d6"
                ],
            },
            {
                "name": "San",
                "films": [
                    "https://ghibliapi.herokuapp.com/films/0440483e-ca0e-4120-8c50-4c8cd9b965d6"
                ],
            },
            {
                "name": "Sosuke",
                "films": [
                    "https://ghibliapi.herokuapp.com/films/758bf02e-3122-46e0-884e-67cf83df1786"
                ],
            },
        ],
        200,
    )
    mocker.patch.object(movies_getter, "client_ghibli", return_value=returned)

    res = correlate_films_with_people(
        {"endpoint": "people", "fields": ["name", "films"]}
    )

    assert {
        "0440483e-ca0e-4120-8c50-4c8cd9b965d6": ["Ashitaka", "San"],
        "758bf02e-3122-46e0-884e-67cf83df1786": ["Sosuke"],
    } == res


def test_get_films_with_people_when_no_correlated_with_people(mocker):
    from movie_list import movies_getter

    mocker.patch.object(movies_getter, "correlate_films_with_people", return_value={})

    res = get_films_with_people({"endpoint": "endpoint", "fields": ["1", "2"]})

    assert [] == res


def test_get_films_with_people_bad_request(mocker):
    from movie_list import movies_getter

    mocker.patch.object(
        movies_getter,
        "correlate_films_with_people",
        return_value={
            "0440483e-ca0e-4120-8c50-4c8cd9b965d6": ["Ashitaka", "San"],
            "758bf02e-3122-46e0-884e-67cf83df1786": ["Sosuke"],
        },
    )
    mocker.patch.object(
        movies_getter, "client_ghibli", side_effect=requests.exceptions.HTTPError
    )

    with pytest.raises(Exception):
        get_films_with_people({"endpoint": "endpoint", "fields": ["1", "2"]})


def test_get_films_with_people(mocker):
    from movie_list import movies_getter

    expected = [
        {
            "id": "0440483e-ca0e-4120-8c50-4c8cd9b965d6",
            "title": "Princess Mononoke",
            "people": ["Ashitaka", "San"],
        },
        {
            "id": "758bf02e-3122-46e0-884e-67cf83df1786",
            "title": "Ponyo",
            "people": ["Sosuke"],
        },
        {
            "id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
            "title": "Castle in the Sky",
            "people": [],
        },
    ]

    mocker.patch.object(
        movies_getter,
        "correlate_films_with_people",
        return_value={
            "0440483e-ca0e-4120-8c50-4c8cd9b965d6": ["Ashitaka", "San"],
            "758bf02e-3122-46e0-884e-67cf83df1786": ["Sosuke"],
        },
    )

    mocker.patch.object(
        movies_getter,
        "client_ghibli",
        return_value=(
            [
                {
                    "id": "0440483e-ca0e-4120-8c50-4c8cd9b965d6",
                    "title": "Princess Mononoke",
                },
                {"id": "758bf02e-3122-46e0-884e-67cf83df1786", "title": "Ponyo"},
                {
                    "id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
                    "title": "Castle in the Sky",
                },
            ],
            200,
        ),
    )

    res = get_films_with_people({"endpoint": "people", "fields": ["id", "title"]})

    assert expected == res
