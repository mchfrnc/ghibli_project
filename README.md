## Ghibli Movie List
This project lists (as a plain text/json) all movies available in Ghibli API. For each movie the people that appear in it are listed

For better visibility, `/movies/` endpoint returns only movie's id, title and list of people. 
All other original fields are skipped.

The information from Ghibli API is cached, the cache time is 60s.

Endpoint `/movies/` allows only `GET` HTTP method.

### How to run the project?
- Build a new image with existing `Dockerfile`, for example, in the project directory:
```bash
docker build --tag ghibli .
``` 
- Run the newly created container, for example:
```bash
docker run -d -p 8000:8000 ghibli
```
- Open a browser and go to the address:
```bash
http://127.0.0.1:8000/movies/
```


### What could be done in further steps?
- Extending `/movies/` endpoint, to have possibility to view a single movie details, e.g. `/movies/<movie_id>/` 
- Turning DEBUG mode off
- Changing `ALLOWED_HOSTS` in settings for better security
- Changing requests' cache for e.g. Redis in place of sqlite file
- After adding more services (like mentioned Redis), usage of `docker-compose` would be beneficial
- Views tests could be extended
